package com.hacker.hank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HankApplication {

	public static void main(String[] args) {
		SpringApplication.run(HankApplication.class, args);
	}

}
